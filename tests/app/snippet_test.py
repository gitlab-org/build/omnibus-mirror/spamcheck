import unittest
from unittest.mock import patch, PropertyMock

import api.v1.spamcheck_pb2 as spam
from app import snippet, config, ValidationError


class MockContext:
    def __init__(self):
        self.correlation_id = "test"


class MockML:
    def __init__(self, score):
        self._score = score

    def score(self, args):
        return self._score

    def set_score(self, score):
        self._score = score


class TestSnippet(unittest.TestCase):
    def test_verdict_user_in_project(self):
        snip = spam.Snippet(title="test", user_in_project=True)
        s = snippet.Snippet(snip, MockContext()).verdict()
        self.assertEqual(
            spam.SpamVerdict.ALLOW, s.verdict, "User in project should be allowed"
        )

    def test_verdict_project_not_allowed(self):
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())
        s._project_allowed = False
        v = s.verdict()
        self.assertEqual(
            spam.SpamVerdict.NOOP, v.verdict, "Disallowed project should return NOOP"
        )

    def test_score(self):
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())

        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.calculate_verdict(0.39),
            "Confidence less than 0.4 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.41),
            "Confidence between 0.4 and 0,5 should be conditionally allowed",
        )
        # Due to valse positives all non allow verdicts were converted to `CONDITIONAL_ALLOW`
        # https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.55),
            "Confidence between 0.5 and 0.9 should be conditionally allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.9),
            "Confidence of 0.9 or greater should be conditionally allowed",
        )

    def test_verdict(self):
        snippet.classifier = MockML(1.0)
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())
        s._project_allowed = False
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            s.verdict().verdict,
            "Disallowed project should return NOOP",
        )
        s._project_allowed = True
        s._email_allowed = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.verdict().verdict,
            "Allowed email should return ALLOW",
        )
        s.user_in_project = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.verdict().verdict,
            "User in project should return ALLOW",
        )
        s._email_allowed = False
        s.user_in_project = False
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.verdict().verdict,
            "ML inference of 1.0 should be conditionally allowed",
        )
        snippet.classifier.set_score(0.1)
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.verdict().verdict,
            "ML inference of 0.1 should be allowed",
        )

    def test_verdict_no_ml(self):
        snippet.classifier = None
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())
        s.project_allowed = True
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            s.verdict().verdict,
            "Snippet ML not loaded should return NOOP",
        )

    def test_validation(self):
        self.assertRaises(
            ValidationError, snippet.Snippet, spam.Snippet(), MockContext()
        )
