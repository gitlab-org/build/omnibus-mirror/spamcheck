import unittest
from unittest.mock import patch, PropertyMock

import api.v1.spamcheck_pb2 as spam
from app import issue, config, ValidationError


class MockContext:
    def __init__(self):
        self.correlation_id = "test"


class MockML:
    def __init__(self, score):
        self._score = score

    def score(self, args):
        return self._score

    def set_score(self, score):
        self._score = score


class TestIssue(unittest.TestCase):
    def test_verdict_user_in_project(self):
        iss = spam.Issue(title="test", user_in_project=True)
        s = issue.Issue(iss, MockContext()).verdict()
        self.assertEqual(
            spam.SpamVerdict.ALLOW, s.verdict, "User in project should be allowed"
        )

    def test_verdict_project_not_allowed(self):
        i = issue.Issue(spam.Issue(title="test"), MockContext())
        i._project_allowed = False
        v = i.verdict()
        self.assertEqual(
            spam.SpamVerdict.NOOP, v.verdict, "Disallowed project should return NOOP"
        )

    def test_score(self):
        s = issue.Issue(spam.Issue(title="test"), MockContext())

        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.calculate_verdict(0.39),
            "Confidence less than 0.4 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.41),
            "Confidence between 0.4 and 0,5 should be conditionally allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.55),
            "Confidence between 0.5 and 0.9 should be disallowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.9),
            "Confidence of 0.9 or greater should be blocked",
        )

    def test_verdict(self):
        issue.classifier = MockML(1.0)
        i = issue.Issue(spam.Issue(title="test"), MockContext())
        i._project_allowed = False
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            i.verdict().verdict,
            "Disallowed project should return NOOP",
        )
        i._project_allowed = True
        i._email_allowed = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            i.verdict().verdict,
            "Allowed email should return ALLOW",
        )
        i.user_in_project = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            i.verdict().verdict,
            "User in project should return ALLOW",
        )
        i._email_allowed = False
        i.user_in_project = False
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            i.verdict().verdict,
            "ML inference of 1.0 should be conditionally allowed",
        )
        issue.classifier.set_score(0.1)
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            i.verdict().verdict,
            "ML inference of 0.1 should be allowed",
        )

    def test_verdict_no_ml(self):
        issue.classifier = None
        i = issue.Issue(spam.Issue(title="test"), MockContext())
        i.project_allowed = True
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            i.verdict().verdict,
            "Issue ML not loaded should return NOOP",
        )

    def test_issue_property(self):
        i = issue.Issue(spam.Issue(title="spam"), MockContext())
        i.allowed_domains = {"gitlab.com"}
        self.assertEqual("spam", i.spammable.title, "Issue should have been set")
        self.assertEqual(False, i._email_allowed, "Blank email should not be allowed")
        args = {"user": {"emails": [{"email": "test@gitlab.com", "verified": True}]}}
        new_issue = spam.Issue(**args)
        i.spammable = new_issue
        self.assertEqual(
            True,
            i._email_allowed,
            "Email should not be allowed after updating issue",
        )

    def test_validation(self):
        self.assertRaises(ValidationError, issue.Issue, spam.Issue(), MockContext())
