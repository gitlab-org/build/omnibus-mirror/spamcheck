##############################
# Build Image
##############################
FROM python:3.9 as builder

ARG UID=1000
ARG GID=1000

WORKDIR /spamcheck

# Add non-root user to run spamchecklications
# Run chmod to ensure everything works if container is run with different uid
RUN groupadd -g $GID spamcheck && \
  adduser \
  --gecos "" \
  --disabled-password \
  --shell "/sbin/nologin" \
  --no-create-home \
  --gid $GID \
  spamcheck && \
  chown -R spamcheck:spamcheck . && \
  chmod -R u=rw,g=rw,o=r,a+X .

USER spamcheck
