#!/usr/bin/env python
import grpc
import api.v1.spamcheck_pb2 as spam
import api.v1.spamcheck_pb2_grpc as spam_grpc

issue_ham = {
    "title": "Dependency update needed",
    "description": "The dependencies for this application are outdated and need to be updated.",
    "user_in_project": False,
    "project": {
        "project_id": 5841855,
        "project_path": "gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck-trigger",
    },
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
    },
}
issue_spam = {
    "title": "fifa xxx porn stream fifa xxx porn stream",
    "description": "fifa xxx porn stream fifa xxx porn stream",
    "user_in_project": False,
    "project": {
        "project_id": 5841855,
        "project_path": "gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck-trigger",
    },
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
    },
}

snippet_ham = {
    "title": " internet time",
    "description": "Swatch Internet Time (or .beat time) is a decimal time concept introduced in 1998 by the Swatch corporation where instead of hours and minutes, the mean solar day is divided into 1000 parts called .beats, with the same time around the globe.",
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
    },
    "files": [
        {
            "path": "beats.py",
        }
    ],
}
snippet_spam = {
    "title": "Deep sea fishing Dubai | deep sea fishing",
    "description": "Beach Riders can be easily accessed from a variety of easily accessible tourist destinations and look to provide you the best [deep sea fishing Dubai](https://www.beachridersdubai.com/activities/sport-deep-sea-fishing-in-dubai/) has to offer.[ Deep sea fishing](https://www.beachridersdubai.com/activities/sport-deep-sea-fishing-in-dubai/) is one of the best activities that you can undertake with your loved ones. ",
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
    },
    "files": [
        {
            "path": "snippetfile1.txt",
        }
    ],
}


def issue_request(issue):
    issue = spam.Issue(**issue)
    with grpc.insecure_channel("localhost:8001") as channel:
        stub = spam_grpc.SpamcheckServiceStub(channel)
        response = stub.CheckForSpamIssue(issue)
    return response


def snippet_request(snippet):
    issue = spam.Snippet(**snippet)
    with grpc.insecure_channel("localhost:8001") as channel:
        stub = spam_grpc.SpamcheckServiceStub(channel)
        response = stub.CheckForSpamSnippet(issue)
    return response


def main():
    print("Checking ham issue")
    response = issue_request(issue_ham)
    print("Verdict: " + response.Verdict.Name(response.verdict))

    print("\nChecking spam issue")
    response = issue_request(issue_spam)
    print("Verdict: " + response.Verdict.Name(response.verdict))

    print("\nChecking ham snippet")
    response = snippet_request(snippet_ham)
    print("Verdict: " + response.Verdict.Name(response.verdict))

    print("\nChecking spam snippet")
    response = snippet_request(snippet_spam)
    print("Verdict: " + response.Verdict.Name(response.verdict))


if __name__ == "__main__":
    main()
