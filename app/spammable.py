"""Logic to perform spam/ham classification"""
import sys
from typing import Any, List

from google.protobuf.json_format import MessageToDict
from vyper import v

from api.v1.spamcheck_pb2 import SpamVerdict
from app import event, logger, queue, data_store
from server.interceptors import SpamCheckContext

log = logger.logger

classifiers = v.get_string("ml_classifiers")
if classifiers:
    sys.path.append(classifiers)

# pylint: disable=too-few-public-methods
class Spammable:
    """Base class for spammable types."""

    allow_list = v.get("filter.allow_list")
    deny_list = v.get("filter.deny_list")
    allowed_domains = set(v.get("filter.allowed_domains"))

    # Currently maximum allowed value is conditional allow
    # to limit false positives.
    _inference_scores = {
        # 0.9: SpamVerdict.BLOCK,
        # 0.5: SpamVerdict.DISALLOW,
        0.4: SpamVerdict.CONDITIONAL_ALLOW,
        0.0: SpamVerdict.ALLOW,
    }

    def __init__(
        self, spammable: Any, context: SpamCheckContext, classifier: None
    ) -> None:
        self.context = context
        self.spammable = spammable
        self.classifier = classifier

    @property
    def spammable(self) -> Any:
        """spam.Spammable: The spammable to analyze for spam"""
        return self._spammable

    @spammable.setter
    def spammable(self, spammable: Any):
        self._spammable = spammable
        self._email_allowed = self.email_allowed(spammable.user.emails)
        if spammable.project:
            self._project_allowed = self.project_allowed(spammable.project.project_id)
        else:
            self._project_allowed = True

    def verdict(self) -> SpamVerdict:
        """Analyze the spammable and determine if spam.

        Returns:
            SpamVerdict
        """
        if not self._project_allowed:
            return self._verdict(SpamVerdict.NOOP, 0.0, "project not allowed")
        if self._email_allowed:
            return self._verdict(SpamVerdict.ALLOW, 0.0, "email allowed")
        if self._spammable.user_in_project:
            return self._verdict(SpamVerdict.ALLOW, 0.0, "user in project")

        if not self.classifier:
            return self._verdict(SpamVerdict.NOOP, 0.0, "classifier not loaded")

        spammable_dict = MessageToDict(self._spammable)
        confidence = self.classifier.score(spammable_dict)
        data_store.save(self.type(), spammable_dict, confidence)
        verdict = self.calculate_verdict(confidence)

        return self._verdict(verdict, confidence, "ml inference score")

    def calculate_verdict(self, confidence: float) -> SpamVerdict:
        """Convert an ML confidence value to a spam verdict.

        Args:
            confidence (float): The ML confidence value

        Returns:
            SpamVerdict
        """
        for threshold, vdict in self._inference_scores.items():
            if confidence >= threshold:
                return vdict

        return SpamVerdict.NOOP

    def _verdict(self, verdict: int, confidence: float, reason: str) -> SpamVerdict:
        fields = {
            "correlation_id": str(self.context.correlation_id),
            "metric": "spamcheck_verdicts",
            "email_allowlisted": self._email_allowed,
            "project_allowed": self._project_allowed,
            "project_path": self._spammable.project.project_path,
            "project_id": self._spammable.project.project_id,
            "user_name": self._spammable.user.username,
            "user_in_project": self._spammable.user_in_project,
            "verdict": SpamVerdict.Verdict.Name(verdict),
            "reason": reason,
            "confidence": confidence,
        }
        log.info("Verdict calculated", extra=fields)
        if verdict not in (SpamVerdict.ALLOW, SpamVerdict.NOOP):
            evnt = event.Event(event.VERDICT, fields)
            queue.publish(evnt)
        return SpamVerdict(verdict=verdict)

    def project_allowed(self, project_id: int) -> bool:
        """Determine if a project should be tested for spam.

        Args:
            project_id (int): The GitLab project ID

        Returns:
            bool
        """
        if len(self.allow_list) != 0:
            if self.allow_list.get(project_id) is not None:
                return True
            return False

        if len(self.deny_list) != 0:
            if self.deny_list.get(project_id) is not None:
                return False
            return True

        return True

    def email_allowed(self, emails: List) -> bool:
        """Determine if a user email should be exempt from spam checking.

        Args:
            emails (list): A list of Emails represented by protobuf objects

        Returns:
            bool
        """
        for email in emails:
            if not "@" in email.email:
                continue
            domain = email.email.split("@")[-1]
            if email.verified and domain in self.allowed_domains:
                return True
        return False

    def type(self) -> str:
        """Get the string representation of the spammable type."""
        return type(self).__name__.lower()
